#include <assert.h>
#include <stdlib.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "device_functions.h"

#include <stdio.h>
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

extern "C" {
#include "bmp.h"
}

__global__ void render(char *out, int width, int height, double x1, double x2, double y1, double y2, int max_iterations) {
	  //Coordonn�es du thread courant attach� � un pixel dans l'image
	  unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	  unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

	  //adresse m�moire du premier canal de couleur pour le pixel courant
	  int index = 3 * width * y + x * 3;


	  double c_r = ((double)x / width) * (x2 - x1) + x1;
	  double c_i = ((double)y / height) * (y2 - y1) + y1;
	  



	  double z_r = 0.0;
	  double z_i = 0.0;

	  int iteration = 0;
	  while(z_r * z_r + z_i * z_i <= 4 && iteration < max_iterations) {
			double temp = z_r;
			z_r = z_r * z_r - z_i * z_i + c_r;
			z_i = 2 * z_i * temp + c_i;
			iteration++;
	  }

	  if(iteration == max_iterations) {
			out[index] = 0;
			out[index + 1] = 0;
			out[index + 2] = 0;
	  } else {
			out[index] = iteration * 0;
			out[index + 1] = iteration * 0;
			out[index + 2] = (double)iteration * 255. / (double) max_iterations ;
	  }
}

void runCUDA(int width, int height, double scale, double speed){
	// Multiply by 3 here, since we need red, green and blue for each pixel
	  size_t buffer_size = sizeof(char) * width * height * 3;

	  char *image;
	  cudaMalloc((void **) &image, buffer_size);

	  char *host_image = (char *) malloc(buffer_size);

	  dim3 blockDim(20, 20, 1);
	  dim3 gridDim(width / blockDim.x, height / blockDim.y, 1);

	  /*//Coordonn�es initiales de la zone regard�e de la fractale
	  double x1 = -2.1;
	  double x2 = 1;
	  double y1 = -1.2;
	  double y2 = 1.2;

	  int max_iterations = 50000;
		
	  
	
	//for (int i = 0; i < scale; ++i) {
		  //Calcul des h � ajouter / retirer des bords du niveau de zoom
		  double dist_x1 = zoom_point_x - x1;
		  double dist_x2 = x2 - zoom_point_x;
		  double dist_y1 = zoom_point_y - y1;
		  double dist_y2 = y2 - zoom_point_y;

		  //Calcul des dimensions de la nouvelle zone apr�s zoom
		  x1 = zoom_point_x - dist_x1 / 3;
		  x2 = zoom_point_x + dist_x2 / 3;
		  y1 = zoom_point_y - dist_y1 / 3;
		  y2 = zoom_point_y + dist_y2 / 3;

		  max_iterations = 1000;
	  //}*/

	  double center_x = -1.985541371654130485531439267191269851811165434636382820704394766801377;
	  double center_y = +0.0000000000000000000000000000015651221211466101983496092509512479178;

	  double length_x = 3;
	  double length_y = 2;

	  double max_iterations = 100;
	 
	  double scale_pow = pow(2., scale * speed);
	 
	  length_x /= scale_pow;
	  length_y /= scale_pow;
	  //max_iterations *= scale_pow;

	  double x1 = center_x - length_x;
	  double x2 = center_x + length_x;
	  double y1 = center_y - length_y;
	  double y2 = center_y + length_y;

	  

	  /*Cr�ation et lancement du timer du kernel */
	  float time;
	  cudaEvent_t start, stop;

	  cudaEventCreate(&start);
	  cudaEventCreate(&stop);
	  cudaEventRecord(start, 0);

	  render<<< gridDim, blockDim >>>(image, width, height, x1, x2, y1, y2, max_iterations);

	  cudaMemcpy(host_image, image, buffer_size, cudaMemcpyDeviceToHost);

	  //Barri�re d'attente de la fin de tous les threads avant de continuer le s�quentielle
	  cudaThreadSynchronize();

	  //Fin du timer et calcul du temps d'exe du kernel
	  cudaEventRecord(stop, 0);
	  cudaEventSynchronize(stop);
	  cudaEventElapsedTime(&time, start, stop);
	  printf(">%s\n", cudaGetErrorString(cudaGetLastError()));


	  printf("%.3f ms \n", time);

	// Now write the file
	string filename = "output";
	if (scale < 10)
		filename += "00";
	else if (scale < 100)
		filename += "0";
	filename += to_string((int)scale) + ".bmp";

	write_bmp(filename.c_str(), width, height, host_image);

	cudaFree(image);
	free(host_image);
}

int main(int argc, const char * argv[]) {
	int nb_frames = 600;
	for (int i = 0; i < nb_frames; ++i) {
		cout << "G�n�ration de l'image : " << i + 1 << " / " << nb_frames << " en : " ;
		runCUDA(1024, 1024, i , 1. / 5.);
	}
	  system("pause");
	  return 0;
}
